# Hola Mundo | TimerCycle

Este proyecto está hecho en base al
[video](https://www.youtube.com/watch?v=aouDQ8caJYg)
en Youtube de
Nicolas Schurmann en su canal
[Hola Mundo](https://www.youtube.com/channel/UC4FHiPgS1KXkUMx3dxBUtPg),
en este caso tomé a bien realizar el ejercicio número 1, ya que era una
prueba de reclutamiento de una empresa en NZ, me esfuerzo mucho todos los dias como
desarrollador con la esperanza de un dia poder migrar ahí, asi que quería saber si ya
tenía el nivel para ello, pero el tiempo de desarrollo en la prueba fue de 4h cuando en la prueba
el tiempo limite era de 1h, fue triste al principio pero luego entendí que debo
seguir mejorando y quien sabe quizás a finales de este año tenga mejores capacidades,
por ahora seguiré el rumbo de React Native.

## Sobre mi

Soy Kevin Rivas tengo 24 años, vivo en El Salvador y soy Ing en Sistemas y
Computación tengo 3 años de programar por cuenta personal y no por una nota o 
por una exigencia, desde hace 3 años eh trabajado como freelance, quiero poder migrar
a un mejor país y por eso me esfuerzo mucho, trabajando como freelance, mientras también
ayudo en un pequeño negocio de mi esposa, y en las noches me desvelo estudiando nuevas tecnologías
o viendo cursos, que por cierto acabo de comprar tu curso
de React Native sin fronteras :p, espero Nicolas que puedas ver mi código
y decirme que piensas de él y en que podría mejorarlo.

## Demo

Puedes ver el 
[demo](https://timer-cycle.vercel.app/)
en Vercel app

## Descripción del problema

El problema consiste en tener un listado de fechas empezando a las 8:00 AM y terminando
a las 8:00 PM, este debe tener un intervalo de 30min entre cada fecha del listado,
se pide:

* Debe contener 8 drivers
* Al dar click en una casilla del tiempo, este debera tomar la posición de un driver
  y restarlo de los 8 disponibles
* Al dar click en la misma casilla este debera liberar el recurso y sumar 1 driver
* al llegar a 0 drivers este debera marcar la casilla en rojo y no dejar tomar el driver
* al transcurrir los 30min(el intervalo) este debera resetear los driver a 8 nuevamente
sin liberar los drivers ya tomados
  
**Note:** Por no haber terminado en 1h la prueba, agregue unos inputs en donde modifica la 
hora inicial(8:00 AM) la hora final(8:00 PM) y el intervalo(30 min) 

## Dependencies

* react: ^17.0.1
* moment: ^2.29.1
* typescript: ^4.0.3
* react-dom: ^17.0.1
* react-scripts: 4.0.1

## Instalación

Usando yarn:

### ```yarn install```

o npm:

### ```npm i```

## Run server

Usando yarn:

### ```yarn start```

o npm:

### ```npm start```

## Arquitectura del proyecto
```
C:.
|   .eslintcache
|   .gitignore
|   package.json
|   README.md
|   tree.txt
|   tsconfig.json
|   yarn.lock
|
+---public
|
\---src
    |   App.test.tsx
    |   App.tsx
    |   constant.ts
    |   index.tsx
    |   react-app-env.d.ts
    |   setupTests.ts
    |
    +---assets
    |   +---fonts
    |   |   |
    |   |   \---iconmoon
    |   |
    |   +---images
    |   |
    |   \---lottie
    |
    +---components
    |
    +---contexts
    |
    +---models
    |
    +---pages
    |   |   index.ts
    |
    +---styles
    |       index.css
    |
    \---utils
```

Espero puedas reaccionar a mi código, un saludo enorme desde
EL Salvador.
