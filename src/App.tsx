import React from 'react';
import Dashboard from "pages/dashboard/dashboard";
import {StateProvider} from "contexts/Global.context";

function App() {
  return <StateProvider>
    <Dashboard/>
  </StateProvider>;
}

export default App;
