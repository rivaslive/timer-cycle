import styles from './style.module.css';
import {GlobalContext} from "contexts/Global.context";
import {getInterceptionList} from "utils/getInterception";
import React, {useCallback, useContext, useEffect, useState} from 'react';

const Board = () => {
  const {globalState, addDrive} = useContext(GlobalContext)
  const [times, setTime] = useState<number[]>([]);
  
  const {currentDriver, notMarked, timer} = globalState;
  
  // get time from loop
  const getTime = useCallback(() => {
    const {interval, start, end} = timer;
    console.log('show new values:')
    let t: any[] = getInterceptionList(interval, start, end)
    console.log(t)
    setTime(t)
  }, [setTime, timer])
  
  // launch event func when change timer in GlobalContext
  useEffect(() => getTime(), [setTime, getTime])
  
  return <div className={`${styles.boardWrapper} beauty-scroll`}>
    <div className={styles.boardRow} style={{height: 65}}>
      <div className={styles.boardHead}>
        <h3>TIME:</h3>
      </div>
    </div>
    {
      times.map((t) => {
        const isMarked = !!currentDriver.find(i => `${t}` === i)
        const isNotChecked = (notMarked === `${t}`)
        return <div
          key={t}
          onClick={() => addDrive(`${t}`)}
          className={
            `${styles.boardRow} ${isMarked ?
              styles.marked : isNotChecked ? styles.notMarked : ''}`
          }
        >
          <div className={styles.boardHead}>
            {/*<span>{(t % 1 === 0) ? `${Math.trunc(t)}:00` : `${Math.trunc(t)}:${(t.toFixed(2)+"").split(".")[1]}`}</span>*/}
            <span>{t}</span>
            {
              isMarked ?
                <i className="fas fa-check-circle text-success"/> :
                isNotChecked && <i className="far fa-times-circle text-danger"/>
            }
          </div>
        </div>
      })
    }
  
  </div>
}

export default Board;
