import React from 'react';
import styles from './styles.module.css'

const Card = ({children, className=''}: IProps) => {
  return <div className={`${styles.cardWrapper} ${className}`}>
    {children}
  </div>
}

interface IProps {
  children: React.ReactNode
  className?: string
}

export default Card;
