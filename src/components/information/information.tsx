import {Card} from "../index";
import React, {useContext} from 'react';
import {GlobalContext} from "contexts/Global.context";

const Information = () => {
  const {globalState} = useContext(GlobalContext)
  const {drivers, currentTime} = globalState;
  
  const status = getStatus(drivers);
  
  return <div className='information'>
    <Card className={status}>
      Motorcycles: {drivers} <i className="fas fa-motorcycle"/>
    </Card>
    <Card>
      Available(New Interval): <span className='text-danger'>{currentTime}</span> <i className="fas fa-stopwatch"/>
    </Card>
  </div>
}


const getStatus = (drivers: number) => {
  if (drivers === 0) return 'danger';
  else if (drivers <= 4) return 'warning';
  return ''
}

export default Information;
