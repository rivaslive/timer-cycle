import {IGlobalState} from "models/interfaces.model";

export const initialStateApp: IGlobalState = {
  drivers: 8,
  currentTime: '0 min',
  currentDriver: [],
  notMarked: '',
  timer: {
    start: 480, // milliseconds
    end: 1200, // milliseconds
    interval: 30  // minutes
  }
}
