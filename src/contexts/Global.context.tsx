import moment from 'moment';
import {initialStateApp} from "constant";
import {getInterception} from "utils/getInterception";
import React, {ReactNode, useEffect, useState} from "react";
import {IContextState, IGlobalState} from "models/interfaces.model";

export const GlobalContext = React.createContext<IContextState>({
  globalState: initialStateApp,
  addDrive: () => {
  },
  setTimer: () => {
  },
});

export const StateProvider = ({children}: { children: ReactNode }) => {
  const [state, setState] = useState<IGlobalState>(initialStateApp)
  const [timeInterceptions, setInterception] = useState<number[]>(getInterception(state.timer.interval))
  
  const resets = () => setState({...state, drivers: initialStateApp.drivers});
  
  const addDrive = (id: string) => {
    const find = state.currentDriver.find(i => i === id)
    /* delete selection? */
    if (!!find) {
      setState({
        ...state,
        drivers: state.drivers + 1,
        currentDriver: state.currentDriver.filter(i => i !== id)
      })
    } else if (state.drivers === 0) setState({...state, notMarked: id});
    else setState({
        ...state,
        notMarked: '',
        currentDriver: [...state.currentDriver, id],
        drivers: state.drivers - 1
      })
  }
  
  const setTimer = (timer: any) => setState({
    ...state,
    drivers: initialStateApp.drivers,
    timer: {...state.timer, ...timer}
  })
  
  const timer = setInterval(() => {
    // Choose the minutes of the current hour
    const minutes = moment().minutes();
    // See if the current minute is equal to the available hours array
    const findInterceptionTime = timeInterceptions.find(i => i === minutes)
    console.log(findInterceptionTime);
    // if current minute is in the list then reset the Drivers
    if (findInterceptionTime) return resets();
    // now I look for the next intersection, to show the time it takes to have more Drivers
    const findNewInterception = timeInterceptions.filter(i => i > minutes)
    // if the intersection exists then the current minutes are subtracted to know exactly how many minutes are left
    const date = !!findNewInterception.length ? `${findNewInterception[0] - minutes} min` : '0 min';
    // Refresh state
    setState({...state, currentTime: date});
  }, 1000);
  
  useEffect(() => {
    return () => {
      clearInterval(timer);
    };
  })
  
  useEffect(() => setInterception(getInterception(state.timer.interval)), [state.timer])
  
  return <GlobalContext.Provider value={{globalState: state, addDrive, setTimer}}>
    {children}
  </GlobalContext.Provider>
};
