export interface IGlobalState {
  drivers: number,
  currentTime: string,
  currentDriver: string[],
  notMarked: string
  timer: {
    start: number
    end: number
    interval: number
  }
}

export interface IContextState {
  globalState: IGlobalState
  addDrive: (val: string) => void
  setTimer: (val: string) => void
}
