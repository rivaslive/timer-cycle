import React from 'react';
import styles from "../styles.module.css";

const FormSetState = ({onChange, start, end, interval}: IProps) => {
  return <div className={styles.form}>
    <label htmlFor="start-date">
      Start Date(Milliseconds) 0 Min- 1440 Max
      <input
        max={1440}
        name='start'
        value={start}
        id='start-date'
        onChange={onChange}
        type="number" min={0}
        placeholder='Start Date'
      />
    </label>
    <label htmlFor="end-date">
      End Date(Milliseconds) 0 Min- 1440 Max
      <input
        min={0}
        name='end'
        max={1440}
        value={end}
        type="number"
        id='end-date'
        onChange={onChange}
        placeholder='End Date'
      />
    </label>
    <label htmlFor="interval-date">
      Interval(Minutes) 0 Min- 60 Max
      <input
        min={0}
        max={60}
        type="number"
        name='interval'
        value={interval}
        id='interval-date'
        onChange={onChange}
        placeholder='Interval'
      />
    </label>
  </div>
}

interface IProps {
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
  start: number
  end: number
  interval: number
}

export default FormSetState;
