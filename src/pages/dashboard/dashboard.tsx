import React, {useContext} from 'react';
import styles from './styles.module.css';
import {Board, Information} from "components";
import {GlobalContext} from "contexts/Global.context";
import FormSetState from "./components/formSetState";

const Dashboard = () => {
  const {globalState, setTimer} = useContext(GlobalContext)
  const {timer} = globalState;
  
  const onChange = ({target}: React.ChangeEvent<HTMLInputElement>) => {
    const value = parseInt(target.value);
    const payload: any = {[target.name]: target.name === 'interval' && value <= 0 ? 1 : value}
    setTimer(payload);
  }
  
  return <div className={styles.wrapper}>
    <div className={styles.body}>
      <h1 style={{textAlign: 'center'}}>Welcome to my Drivers</h1>
      <div className={styles.content}>
        <div>
          <Information/>
          <FormSetState {...timer} onChange={onChange}/>
        </div>
      </div>
    </div>
    <div className={styles.board}>
      <Board/>
    </div>
  </div>
}

export default Dashboard;
