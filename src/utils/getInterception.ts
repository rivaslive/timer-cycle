import moment from "moment";

export const getInterception = (step: number) => {
  const minutes = moment().minutes();
  let interception: number[] = []
  for (let i = step; i <= 60; i = i + step) {
    if (minutes !== i) interception = [...interception, i];
  }
  return interception
}

export const getInterceptionList = (step: number, start: number, end: number) => {
  let times = []; // time array
  let startTime = start; // start time
  const ap = ['AM', 'PM']; // AM-PM

//loop to increment the time and push results in array
  for (let i = 0; startTime <= end; i++) {
    const hh = Math.floor(startTime / 60); // gestartTimeing hours of day in 0-24 format
    const mm = (startTime % 60); // gestartTimeing minutes of the hour in 0-55 format
    times[i] = ("0" + (hh % 12)).slice(-2) + ':' + ("0" + mm).slice(-2) + ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
    startTime = startTime + step;
  }
  
  return times
}
